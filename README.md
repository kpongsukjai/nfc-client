## Prerequisites
1. [NodeJS](https://nodejs.org/dist/v12.18.3/node-v12.18.3-x64.msi)
2. Installing serve (Using command: `npm install -g serve`)
3. npm install

## Setup environment parameter
You can change admin url and server url in file .env-cmdrc

## Setup for development
1. npm start

## Deployment
1. npm run build
2. Move `build` folder from root project to some destination in server (e.g. `c:/`)
3. Go `c:/` (In your server) then change folder name from `build` to `nfc-client`
4. Use command `serve -l 3000 -s nfc-client`