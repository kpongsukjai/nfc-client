import React from 'react'
import './registration.css'
class Registration extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            code: this.props.code,
            event: localStorage.getItem('event'),
            status: this.props.status,
            checkOutResult: this.props.checkOutResult,
            registerResult: null,
            zones: this.props.zones,
            consents: this.props.consents,
            ageRanges: this.props.ageRanges,
            selectedZone: localStorage.getItem('zone')
        }
    }
    componentDidUpdate() {
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.checkOutResult && nextProps.checkOutResult !== this.state.checkOutResult) {
            this.setState({
                checkOutResult: nextProps.checkOutResult
            })
        }
        if (nextProps.status && nextProps.status !== this.state.status) {
            this.setState({
                status: nextProps.status
            })
        }
        if (nextProps.zones && nextProps.zones !== this.state.zones) {
            this.setState({
                zones: nextProps.zones
            })
        }
    }
    componentWillMount() {
        if (this.state.status === 'checkout') {
            // this.onCheckout()
        }
    }
    validateMobile = (mobileNo) => {
        const startsWith = (text, prefix) => {
            return text.indexOf(prefix) === 0
        }

        if (!(/^\d{10}$/.test(mobileNo))) {
            return false
        }
        var prefixList = [
            '04',
            '05',
            '061',
            '062',
            '063',
            '064',
            '065',
            '066',
            '07',
            '08',
            '09'
        ]
        for (let i = 0; i < prefixList.length; i++) {
            if (startsWith(mobileNo, prefixList[i])) {
                return true
            }
        }
        return false
    }
    disableBtn = (val) => {
        this.refs['register-btn'].disabled = val
    }
    onSubmitForm = async () => {
        this.disableBtn(true)
        const { name, mobileNo, zones, surname, age } = this.refs
        const form = {
            tagId: this.state.code.signature,
            name: name.value,
            mobileNo: mobileNo.value,
            eventId: this.state.event,
            status: this.state.status,
            zone: zones.value,
            surname: surname.value,
            consents: [],
            gender: 0,
            age: age.value
        }
        const consents = this.state.consents
        form.consents = consents.filter((consent) => {
            if (consent.id == 0 && consent.text) {
                return true
            }
            let element = this.refs['consent-' + consent.id]
            if (element) {
                return element.checked
            }
            return false
        })

        for (let i in form.consents) {
            form.consents[i].accepted_time = new Date().getTime()
        }

        const genderElements = document.getElementsByName('gender')
        for (let i in genderElements) {
            if (genderElements[i].checked) {
                form.gender = genderElements[i].value
            }
        }
        if (!form.name) {
            this.disableBtn(false)
            alert('Please enter a name.')
            return
        }
        if (!this.validateMobile(form.mobileNo)) {
            this.disableBtn(false)
            alert('Mobile No. is invalid format.')
            return
        }

        if (form.zone === undefined) {
            this.disableBtn(false)
            alert('Zone is invalid.')
            return
        }

        let result = await window.nfc.post('register', form)
        if (!result.error) {
            window.document.body.scrollTop = 0
            window.document.documentElement.scrollTop = 0
            this.setState({
                registerResult: result
            })
        } else {
            // this.props.onChangeMenu('main')
            this.disableBtn(false)
        }
    }
    onCheckout = async () => {
        let result = await window.nfc.post('register', {
            tagId: this.state.code.signature,
            eventId: this.state.event,
            status: this.state.status
        })
        if (!result.error) {
            this.setState({
                checkOutResult: result
            })
        } else {
            window.location = window.location.href.split("?")[0]
        }
    }
    getZoneSelector = () => {
        let zones = this.state.zones
        let selectedZone
        for (let i in zones) {
            if (zones[i].created_time == this.state.selectedZone) {
                selectedZone = this.state.selectedZone
                break
            }
        }
        return <select defaultValue={selectedZone} ref="zones" onChange={(e) => {
            localStorage.setItem('zone', e.target.value)
        }}>
            <option value={null}>Not Zoned</option>
            {
                zones.map((zone) => {
                    return <option value={zone.created_time}>{zone.name}</option>
                })
            }
        </select>
    }
    getAgeRanges = () => {
        const { ageRanges } = this.state
        return <select ref="age" className="age-selector">
            <option value={'None'}>None</option>
            {
                ageRanges.map((age) => {
                    return <option value={age.text}>{age.text}</option>
                })
            }
        </select>
    }
    getConsents = () => {
        let { consents } = this.state
        consents = consents.filter(consent => consent.text)
        return consents.map((consent) => {
            return <tr>
                <td className="consent-cell">
                    <input ref={"consent-" + consent.id} id={"consent-" + consent.id} type="checkbox" />
                    <label htmlFor={"consent-" + consent.id}>
                        &nbsp;{consent.text}
                    </label>
                </td>
            </tr>
        })
    }
    render() {
        if (this.state.status === 'checkout') {
            return <div className="registration">
                {
                    this.state.checkOutResult ?
                        <div>
                            {this.state.checkOutResult} is logged out.
                    </div>
                        :
                        <div>
                            Please wait...
                    </div>
                }
            </div>
        } else if (this.state.registerResult) {
            const result = this.state.registerResult
            return <div className="registration">
                <div className="registration-login-succes">
                    Register successfully
                </div>
                <div>
                    <center>
                        <table className="table-result">
                            <thead>
                                <tr>
                                    <td>Zone</td>
                                    <td>{result.zone}</td>
                                </tr>
                                <tr>
                                    <td>Tag ID</td>
                                    <td>{result.tagId}</td>
                                </tr>
                                <tr>
                                    <td>Name</td>
                                    <td>{result.name}</td>
                                </tr>
                                <tr>
                                    <td>Mobile No.</td>
                                    <td>{result.mobileNo}</td>
                                </tr>
                            </thead>
                        </table>
                    </center>
                </div>
                <div>
                    <button className="registration-btn-back" onClick={() => { this.props.onChangeMenu('main') }}>back to main</button>
                </div>
            </div>
        }
        return <div className="registration">
            <center>
                <table className="table-register">
                    <tr>
                        <td>Registration</td>
                    </tr>
                    {
                        localStorage.getItem('topic') ?
                            <tr>
                                <td>
                                    {localStorage.getItem('topic')}
                                </td>
                            </tr>
                            :
                            null
                    }
                    <tr>
                        <td>{this.getZoneSelector()}</td>
                    </tr>
                    <tr>
                        <td>
                            Tag ID
                    </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" value={this.state.code.signature} disabled={true} />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Name
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input ref="name" type="text" autoFocus={true} />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Surname <span className="optional">(optional)</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input ref="surname" type="text" />
                        </td>
                    </tr>
                    <tr>
                        <td>Mobile No.</td>
                    </tr>
                    <tr>
                        <td>
                            <input ref="mobileNo" type="number" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Gender</span>
                            <span>
                                <input name="gender" value="None" id="none" ref="genderNone" type="radio" defaultChecked={true} />
                                <label htmlFor="none">
                                    None
                                    </label>
                            </span>
                            <span>
                                <input name="gender" value="Male" id="male" ref="genderMale" type="radio" />
                                <label htmlFor="male">
                                    Male
                                    </label>
                            </span>
                            <span>
                                <input name="gender" value="Female" id="female" ref="genderFemale" type="radio" />
                                <label htmlFor="female">
                                    Female
                                    </label>
                            </span>
                        </td>
                    </tr>
                    {
                        this.state.ageRanges && <tr>
                            <td className="age-cell">
                                Age {this.getAgeRanges()}
                            </td>
                        </tr>
                    }
                    {
                        this.state.consents && this.getConsents()
                    }
                    <tr>
                        <td>
                            <br />
                            <center>
                                <button ref='register-btn' onClick={this.onSubmitForm} className="registration-btn-submit">
                                    Submit
                                </button>
                            </center>
                        </td>
                    </tr>
                </table>
            </center>
        </div>

        return <div className="registration">
            <div className="registration-header">
                <div className="header">
                    Registration
                </div>
                <div className="sub-header">
                    {localStorage.getItem('topic')}
                </div>
            </div>
            <div className="registration-form">
                <div className="registration-line">
                    <div className="registration-line-input">
                        {this.getZoneSelector()}
                    </div>
                </div>
                <div className="registration-line">
                    <div className="registration-line-label">
                        Tag ID
                    </div>
                    <div className="registration-line-input">
                        <input type="text" value={this.state.code.signature} disabled={true} />
                    </div>
                </div>
                <div className="registration-line">
                    <div className="registration-line-label">
                        Name
                    </div>
                    <div className="registration-line-input">
                        <input ref="name" type="text" autoFocus={true} />
                    </div>
                </div>
                <div className="registration-line">
                    <div className="registration-line-label">
                        Mobile No.
                    </div>
                    <div className="registration-line-input">
                        <input ref="mobileNo" type="number" />
                    </div>
                </div>
                <div className="registration-line">
                    <button ref='register-btn' onClick={this.onSubmitForm} className="registration-btn-submit">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    }
}

export default Registration