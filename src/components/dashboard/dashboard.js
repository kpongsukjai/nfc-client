import React from 'react'
import './dashboard.css'

class Dashboard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            event: this.props.event,
            name: this.props.name,
            zone: this.props.zone,
            count: 'N/A',
            size: 'N/A',
            closed: false
        }
    }
    componentWillMount() {
        console.log(this.state)
        this.realtime()
    }
    realtime = () => {
        window.nfc.post('realtime', {
            selectedEvent: this.state.event.created_time,
            selectedZone: this.state.zone,
            fastMode: true
        }, true).then((result) => {
            if (!result.error) {
                this.setState({
                    count: result.count,
                    size: result.size,
                    closed: false
                })
            } else {
                this.setState({
                    closed: true
                })
            }
        })
        setTimeout(() => {
            this.realtime()
        }, 1000)
    }
    colorStatus = () => {
        return this.state.count == this.state.size ? 'red' : 'green'
    }
    render() {
        return <div className="dashboard-section">
            {
                this.state.closed ?
                    <div className="event-topic">
                        {this.state.name} is closed.
                    </div>
                    :
                    [
                        <div className="event-topic">
                            {this.state.event.topic}<br/>{this.state.name}
                        </div>,
                        <div className={"event-count " + this.colorStatus()}>
                            {this.state.count}
                        </div>,
                        <div className={"event-line"}>
                            <div className={"line-element " + this.colorStatus()}>

                            </div>
                        </div>,
                        <div className={"event-size " + this.colorStatus()}>
                            {this.state.size}
                        </div>
                    ]
            }
        </div>
    }
}

export default Dashboard