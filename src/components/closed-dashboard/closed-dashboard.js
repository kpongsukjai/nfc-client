import React from 'react'
import './closed-dashboard.css'

class ClosedDashboard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            event: this.props.event,
            url: this.props.url
        }
    }
    render() {
        return <div className="closed-dashboard-section">
            <div className="closed-space">
                &nbsp;
            </div>
            <div className="closed-label">
                {this.state.event} is closed
            </div>
            {
                this.state.url ?
                    <div className="closed-url">
                        Click <a href={this.state.url}>here</a> to go Event Gallery
                    </div>
                    :
                    ''
            }
        </div>
    }
}

export default ClosedDashboard