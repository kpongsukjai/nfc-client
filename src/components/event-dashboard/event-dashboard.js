import React from 'react'
import './event-dashboard.css'

class EventDashboard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            event: this.props.event,
            zones: []
        }
    }
    componentWillMount() {
        this.getZones()
    }
    getZones = async () => {
        console.log(this.state)
        let result = await window.nfc.post('getZonesCount', {
            created_time: this.state.event.created_time
        })
        if (!result.error) {
            this.setState({
                zones: result
            })
        }
    }

    getZonesItems = () => {
        let zones = this.state.zones
        return zones.map((zone) => {
            let color = ''
            if (zone.size == zone.count) {
                color = 'red'
            }
            return <div className={"event-dashboard-item " + color}>
                <div className="zone-name">
                    {zone.name}
                </div>
                <div className="zone-capacity">
                    {zone.count} / {zone.size}
                </div>
            </div>
        })
    }

    render() {
        return <div className="event-dashboard-section">
            <div className="event-dashboard-header">
                {this.state.event.name}
            </div>
            <div className="event-dashboard-body">
                {this.getZonesItems()}
            </div>
        </div>
    }
}

export default EventDashboard