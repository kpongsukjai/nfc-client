import React from 'react'
import './client-dashboard.css'
class ClientDashboard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            events: this.props.events
        }
    }
    getEvents = () => {
        let events = this.state.events
        return events.map((event) => {
            return <div onClick={() => { window.selectClientEvent(event) }} className="client-event-item">
                {event.name}
            </div>
        })
    }
    render() {
        return <div className="client-events-section">
            <div className="client-events-header">
                Please select an event
            </div>
            <div className="client-events-body">
                {
                    this.getEvents()
                }
            </div>
        </div>
    }
}

export default ClientDashboard