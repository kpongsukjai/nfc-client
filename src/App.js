import React from 'react'
import './App.css'
import Registration from './components/registration/registration'
import Dashboard from './components/dashboard/dashboard'
import EventDashboard from './components/event-dashboard/event-dashboard'
import ClientDashboard from './components/client-dashboard/client-dashboard'
import ClosedDashboard from './components/closed-dashboard/closed-dashboard'
import Connection from './services/connector'
import nfcIcon from './assets/nfc-icon-2.png'

const managerUrl = '' // do not use for right now

const connection = new Connection()
window.nfc = connection

class App extends React.Component {
  constructor(props) {
    super(props)
    let localEvent = localStorage.getItem('event')
    let localTopic = localStorage.getItem('topic')
    let localZone = localStorage.getItem('zone')
    let localName = localStorage.getItem('name')
    let localState = localStorage.getItem('state')
    let localEventObject = undefined
    try {
      if (localEvent && localTopic) {
        localEventObject = {
          created_time: localEvent,
          topic: localTopic
        }
      }
    } catch (e) {
      console.log(e)
    }
    this.state = {
      code: undefined,
      event: localEventObject || undefined,
      menu: 'main', // main, dashboard, registration
      status: undefined,
      checkOutResult: undefined,
      zones: [],
      events: [],
      state: localStorage,
      zone: localZone,
      name: localName,
      state: localState,
      message: '',
      url: '',
      consents: [],
      ageRanges: []
    }
  }
  componentWillMount() {
    this.getUrlParams()
    window.openEventManager = this.openEventManager
    window.selectClientEvent = this.selectClientEvent
  }
  getSignatureStatus = async (code) => {
    let result = await window.nfc.post('getSignatureStatus', {
      code: code,
      event: this.state.event
    })
    if (!result.error) {
      if (result.status === 'valid') {
        this.setState({
          code: code,
          menu: 'registration',
          status: result.status,
          zones: result.zones,
          ageRanges: result.ageRanges,
          consents: result.consents
        })
      } else if (result.status === 'client') {
        if (result.data.length === 1) {
          this.selectClientEvent(result.data[0])
        } else {
          this.setState({
            menu: 'client-dashboard',
            events: result.data
          })
        }
      } else if (result.status === 'closed') {
        this.setState({
          menu: 'closed-dashboard',
          url: result.url,
          event: result.event
        })
      }
    }
  }
  selectClientEvent = (event) => {
    this.setState({
      menu: 'event-dashboard',
      event: event
    })
  }
  scan = async (code) => {
    let result = await window.nfc.post('scan', {
      event: this.state.event,
      code: code,
      zone: this.state.zone
    })
    if (!result.error) {
      this.setState({
        state: 'scanner',
        menu: 'main',
        closed: false,
        message: {
          status: result.status,
          text: result.text
        }
      })
      setTimeout(() => {
        this.setState({
          state: 'scanner',
          message: null
        })
        this.realtime()
      }, 2000)
    }
  }
  realtime = (event, zone) => {
    event = event || this.state.event
    zone = zone || this.state.zone
    window.nfc.post('realtime', {
      selectedEvent: event.created_time,
      selectedZone: zone,
      fastMode: true
    }, true).then((result) => {
      if (!result.error) {
        this.setState({
          count: result.count,
          size: result.size,
          closed: false
        })
      } else {
        this.setState({
          closed: true
        })
      }
    })
    setTimeout(() => {
      this.realtime()
    }, 1000)
  }
  getUrlParams = () => {
    const urlParams = new URLSearchParams(window.location.search)
    window.history.replaceState({}, document.title, "/")
    for (let key of urlParams.keys()) {
      const text = urlParams.get(key)
      if (key === 'sig') {
        const code = {
          signature: text.slice(0, 14),
          fullCode: text
        }
        this.setState({
          menu: 'scanning'
        })
        if (this.state.state === 'register') {
          this.getSignatureStatus(code)
        } else if (this.state.state === 'scanner') {
          this.scan(code)
        } else {
          this.getSignatureStatus(code)
        }
        break
      } else if (key === 'event') {
        const topic = urlParams.get('topic')
        const status = urlParams.get('status')
        const token = urlParams.get('token')
        const zone = urlParams.get('zone')
        const name = urlParams.get('name')
        const state = urlParams.get('state')
        const event = {
          created_time: text,
          topic: topic
        }
        localStorage.setItem('event', text)
        localStorage.setItem('topic', topic)
        localStorage.setItem('token', token)
        localStorage.setItem('zone', zone)
        localStorage.setItem('name', name)
        localStorage.setItem('state', state)
        this.setState({
          event: event,
          status: status,
          name: name,
          zone: zone,
          state: state
        })
        if (state === 'scanner') {
          this.realtime(event, zone)
        }
      } else if (key === 'menu') {
        this.setState({
          menu: urlParams.get('menu')
        })
      } else if (key === 'state') {
        this.setState({
          state: urlParams.get('state')
        })
      }
    }
  }
  resetEvent = () => {
    localStorage.clear()
    window.location.href = managerUrl + '?activeEvent=' + this.state.event.created_time
  }
  onChangeMenu = (menu) => {
    this.setState({ menu })
  }
  openEventManager = () => {
    window.location.href = managerUrl
  }

  getStateLabel = () => {
    const { state } = this.state
    if (state === 'scanner') {
      return <div className="scanner-section">
        <div className="scanner-row">
          <div className="scanner-cell">
            {this.state.event.topic}
          </div>
        </div>
        <div className="scanner-row">
          <div className="scanner-cell">
            {this.state.name}
          </div>
        </div>
        {
          this.state.closed === true ?
            <div className="scanner-closed">
              <div className="scanner-cell">
                This event is closed.
            </div>
            </div>
            :
            null
        }
        {
          this.state.closed === undefined || this.state.closed === true ?
            null
            :
            (
              this.state.message ?
                <div className="scanner-message">
                  <div className="scanner-cell">
                    {this.state.message.text}
                  </div>
                </div>
                :
                [<div className="scanner-row">
                  <div className="scanner-cell">
                    Participants&emsp;<span className="size-text">{this.state.count}/{this.state.size}</span>
                  </div>
                </div>,
                <div className="scanner-row">
                  <div className="scanner-cell">
                    <img src={nfcIcon} />
                  </div>
                </div>,
                <div className="scanner-row">
                  <div className="scanner-cell">
                    Please tab to <b>Enter</b> or <b>Exit</b>
                  </div>
                </div>]
            )
        }
      </div>
    } else if (state === 'register') {
      return <div className="register-section">
        <div className="register-row">
          <div className="register-cell">
            {this.state.event.topic}
          </div>
        </div>
        <div className="register-row">
          <div className="register-cell">
            <img src={nfcIcon} />
          </div>
        </div>
        <div className="register-row">
          <div className="register-cell">
            Please tap to check register
          </div>
        </div>
      </div>
    }
  }

  getMessage = () => {
    let { message } = this.state
    return <div className="message">
      {message.text}
    </div>
  }

  render() {
    return <div className="app">
      {
        this.state.menu === 'message' ?
          this.getMessage() :
          null
      }
      {
        this.state.menu === 'scanning' ?
          ''
          :
          null
      }
      {
        this.state.menu === 'main' && this.state.state ?
          (
            this.state.event ?
              <div className="icon">
                {this.getStateLabel()}
              </div>
              :
              <div className="event-activated">
                <div>
                  Please enter from event manager
                </div>
                <div>
                  <div className="button-area">
                    <button onClick={this.openEventManager}>
                      Event Manager
                      </button>
                  </div>
                </div>
              </div>
          )
          :
          null
      }
      {
        this.state.menu === 'dashboard' ?
          <Dashboard event={this.state.event} zone={this.state.zone} name={this.state.name} />
          :
          null
      }
      {
        this.state.menu === 'registration' ?
          <Registration zones={this.state.zones} consents={this.state.consents} ageRanges={this.state.ageRanges} checkOutResult={this.state.checkOutResult} status={this.state.status} onChangeMenu={this.onChangeMenu} code={this.state.code} />
          :
          null
      }
      {
        this.state.menu === 'client-dashboard' ?
          <ClientDashboard events={this.state.events} />
          :
          null
      }
      {
        this.state.menu === 'event-dashboard' ?
          <EventDashboard event={this.state.event} />
          :
          null
      }
      {
        this.state.menu === 'closed-dashboard' ?
          <ClosedDashboard event={this.state.event} url={this.state.url} />
          :
          null
      }
    </div>
  }
}

export default App
